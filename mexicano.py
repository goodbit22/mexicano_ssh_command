#!/usr/bin/env python3
from termcolor import colored
from ssh_session import SSH
from termcolor import colored
from pyfiglet import figlet_format
from os import path, getuid
from logging import warning, error
from getpass import getpass
from addition_func import create_directory, read_script_bash, read_script_powershell, ssh_available, os_detection, insert_ipAddress, read_filename
from addition_func import search_host_in_file

def user_commands(host, user, password, port):
    session = SSH(host, user, password, port)
    command=''
    status=None
    while True:
        if status == False:
            print(colored("SSH connection closed", "green"))
            break
        print("If I want to leave the session, type the command [exit]")
        command = input("Enter command:  ")
        if command == 'exit':
            print(colored("SSH connection closed", "green"))
            break
        status = session.ssh_command(command)
    session.ssh_close()

def run_command_from_file(commands, session):
     for command in commands:
        print(colored(f"execute command: {command}", "blue"))
        status = session.ssh_command(command)
        if status == False:
            break

def sub_choose(host, user, password, port):
    def sub_menu():
        print("\n")
        print(colored("------------------------ Submenu ------------------------", "red"))
        print(
        """
        1)Run commands from a file
        2)Enter command
        3)Back
        """
        )
    while True: 
        print("\n" * 100)
        sub_menu()
        option = str(input("Select options: "))
        if option == '1':
            session = SSH(host, user, password, port)
            system_platform = os_detection(host)
            try:
                if system_platform == 'Linux':
                    commands=read_script_bash(read_filename("sh"))
                    run_command_from_file(commands, session)
                elif system_platform == 'Windows':
                    commands=read_script_powershell(read_filename("ps1"))
                    run_command_from_file(commands, session)
                elif system_platform == 'Solaris':
                    commands=read_script_bash(read_filename("sh"))
                    run_command_from_file(commands, session)
            except KeyboardInterrupt:
                session.ssh_close()
                print("EXIT")
                exit(0)
            session.ssh_close()
            break
        elif option == '2':
            system_platform = os_detection(host)
            if system_platform == 'Linux':
                user_commands(host, user, password, port)
            elif system_platform == 'Windows':
                user_commands(host, user, password, port)
            elif system_platform == 'Solaris':
                user_commands(host, user, password, port)
            break
        elif option == '3':
            print("\n" * 100)
            break
        else:
            print("\n" * 100)

def address_option_only():
    host=insert_ipAddress()
    user, password, port = search_host_in_file(host)
    if user and password and port:
        sub_choose(host, user, password, port)
    else:
        warning(colored(f"The ip address {host} is not in the correct_pass_ssh.txt file","yellow"))

def all_data_option():
    host=insert_ipAddress()
    while True:
        try:
            port = int(input("Please enter the port number: "))
            user = input("Please enter the username: ")
            password = getpass()
            ssh = ssh_available(host, port)
            if ssh:
                sub_choose(host, user, password, port)
                break
            else:
                warning(colored('The ssh service is not available or is running on a different port.', 'yellow'))
                break
        except ValueError:
            print(colored("Not a valid port number","yellow"))        

def main():
    def main_menu():
        print(colored("\n------------------------ Main Menu Mexicano ------------------------", "magenta"))
        print(
        """
        1)Enter only the ip address
        2)Enter all data
        3)Exit
        """
        )
    option=22
    while option != '3': 
        main_menu()
        option = str(input("Select options: "))
        if option == '1':
            address_option_only()
        elif option == '2':
            all_data_option()
        elif option  == '3':
            print(colored("EXIT", "blue"))
            return 0
        else:
            print("\n" * 100)

if __name__ == '__main__':
    if getuid() == 0:
        ascii_banner = figlet_format("Mexicano")
        print(colored(ascii_banner, 'red', attrs=['reverse', 'blink']))
        print(colored("Created by goodbit22", "green")) 
        create_directory()
        try:
            main()
        except KeyboardInterrupt:
            print(colored("EXIT", "blue"))
    else:
        error(colored("You do not have administrator rights, you need to run the script as administrator", "red"))