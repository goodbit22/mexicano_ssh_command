from os import path, mkdir
from socket import socket, AF_INET, SOCK_STREAM
from logging import info, warning
from subprocess import Popen, PIPE
from ipaddress import ip_address
from re import search
import nmap

def read_filename(type_script):
    filename = input("Enter filename from commands: ")
    pattern=rf"^\S*.{type_script}$"
    if search(pattern, filename):
        return filename
    return 'ssh_command_linux.sh' if type_script == 'sh' else 'ssh_command_win.ps1'

def read_script_bash(filename_bash='ssh_command_linux.sh'):
    if not path.exists(filename_bash):
        with open(filename_bash, 'w') as create_file_bash:
            create_file_bash.write("echo 'game over'")
    with open(filename_bash,'r') as file_bash:
        bash_command=[command.replace('\n','') for command in file_bash]
    return bash_command

def read_script_powershell(filename_powershell='ssh_command_win.ps1'):
    if not path.exists(filename_powershell):
        with open(filename_powershell, 'w') as create_file_powershell:
            create_file_powershell.write("Write-Host 'Game over'")
    with open(filename_powershell,'r') as file_powershell:
        powershell_command=[command.replace('\n','') for command in file_powershell]
    return powershell_command

def create_directory(directory_name="ssh_commands_result"):
    try:
        if not path.isdir(directory_name):
            mkdir(directory_name)
    except OSError as error: 
        print(error)  

def ssh_available(ip_address, number_port=22):
    sock = socket(AF_INET, SOCK_STREAM)
    port = sock.connect_ex((ip_address, number_port))
    if port == 0:
        sock.close()
        info(f"[+] Port {number_port} is open")
        return True
    else:
        sock.close()
        warning(f"[-] Port {number_port} is not open")
        return False

def os_detection(ip_address='127.0.0.1'):
    nm = nmap.PortScanner()
    machine = nm.scan(ip_address, arguments='-O')
    return machine['scan'][ip_address]['osmatch'][0]['osclass'][0]['osfamily']

def insert_ipAddress():      
        def check_ip(ip_addr):
            process = Popen(['ping', '-c', '2', ip_addr ], stderr=PIPE, stdout=PIPE)
            response=process.wait()
            return response
        status=None
        while status != 0:
            while True:
                try:
                    host = input("Please enter the ip address: ")
                    ip_address(host)
                    break
                except ValueError:
                    print("Not a valid IP address")
            status = check_ip(host)
            if status == 0:
                print("Status: up ")
                return host
            else:
                print("Status: down")

def search_host_in_file(host):
    create_directory('result_dictionary_attack')
    def read_file_save_correct_password(host, filename_correct_pass='result_dictionary_attack/correct_pass_ssh.txt'):
        if not path.exists(filename_correct_pass):
            with open(filename_correct_pass, 'w') as create_file_bash:
                pass
        pattern=r":\S*"
        with open(filename_correct_pass, 'r') as file_correct_pass:
            #ssh_data= [dat.replace('\n','') for dat in file_correct_pass if host+'\n' == sub(pattern, '', dat)]
            ssh_data = [dat for dat in file_correct_pass]
        #print(ssh_data)
        return ssh_data  
    ssh_data = read_file_save_correct_password(host)
    if ssh_data:
            host, user, password, port = str(ssh_data[0]).split(':')
            return user, password, port
    return None, None, None