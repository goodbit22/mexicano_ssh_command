# mexicano_ssh_command

This script allows you to execute commands using the ssh protocol

![graphic1](./img/1.png)

![graphic2](./img/2.png)

## Table of Contents

- [mexicano_ssh_command](#mexicano_ssh_command)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Instalation](#instalation)
  - [Running](#running)
  - [Technologies](#technologies)
  - [Authors](#authors)
  - [License](#license)

## Features

- Executing Linux commands from files  with the extension .sh and save the command results to a file
- Executing Windows commands from file with the extension .ps1 and save the command results to a file
- Executing Linux commands entered directly by the user and save the the command resultsto a file
- Executing Windows commands entered directly by the user and save the command results to a file
- When the user enters the address, it checks if the ip address is available and has a good ipv4 format.
- The script checks the types of operating systems
- logging by enter only the ip address, if the login information is in the correct_pass_ssh file
- logging by Enter all data (host, user, password, port)

## Instalation

Before running the script for the first time, run the commands

```python3
 pip3 install -r requirements.txt
```

```sh
  sudo apt install python3-termcolor
```

## Running

```python3
 python3 mexicano.py
```

## Technologies

Project is created with:

- python 3.9.7
- paramiko
- nmap

## Authors

***
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
***

## License

   Apache License Version 2.0
