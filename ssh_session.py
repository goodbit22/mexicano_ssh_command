#! /usr/bin/env python3
from paramiko import AutoAddPolicy, SSHClient, ssh_exception
from datetime import datetime
from logging import INFO, getLogger
class SSH:
    def __init__(self, host, user, password, port):
        self.host = host
        self.user = user
        self.password = password
        self.port = port
        self.name_file = '_'.join((host, user, password, str(port), datetime.now().strftime("%H:%M:%S") )) + '.txt'
        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        self.logger = getLogger()
        self.logger.setLevel(INFO)

    def ssh_command(self, command):
        self.command = command
        self.ssh_stdin = self.ssh_stdout = self.ssh_stderr = None
        try:
            self.client.connect(self.host, username=self.user, password=self.password)
            #self.client.connect(self.host, port=22,username=self.user, password=self.password)
            ssh_stdin, ssh_stdout, ssh_stderr = self.client.exec_command(self.command)
            ssh_stdout.channel.recv_exit_status()
            output = ssh_stdout.readlines() + ssh_stderr.readlines()
            with open('ssh_commands_result/'+self.name_file, 'a+') as file_result:
                if output:
                    print('--- Output ---')
                    file_result.write('--- Output ---\n')
                    for line in output:
                        print(line.strip())
                        file_result.write(line)
                return True
        except ssh_exception.SSHException as ex:
            print("SSH connection error: {0}".format(ex))
            return False
    
    def ssh_close(self):
        if self.client:
            self.client.close()