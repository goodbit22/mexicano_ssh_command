Write-Host "Game over"
Write-Host "Informacje o sieciowie"  -ForegroundColor Green
ipconfig
netstat -bnao 
netstat -r 
net share 
net view /Domain
net view
Write-Host "Informacje o uzytkownikach" -ForegroundColor Green
net user
net accounts
gwmi win32_useraccount 
whoami /groups 
whoami /all
Get-Acl
gwmi win32_useraccount
Write-Host "Informacje o systemie" -ForegroundColor Green
Get-HotFix   
get-host | Select-Object version 
Write-Host "Informacje ogolne" -ForegroundColor Green
Get-Partition
Get-Location
$env:computername
Write-Host "Informacje inne" -ForegroundColor Green
Get-Servicek
Get-ExecutionPolicy -List
Get-Process
Get-EventLog -List
Get-Service | Get-Member